package TrabajoIndividualProcesadorDeTexto;

import javax.swing.*;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Main {

    public static void main(String[] args) {

        MarcoFrame marco=new MarcoFrame();
        marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
class MarcoFrame extends JFrame{

    public MarcoFrame() {

        setTitle("Interfaz gráfica");
        setBounds(400,200,600,400);
        LaminaMenu miLamina = new LaminaMenu();
        add(miLamina);

        Toolkit sist = Toolkit.getDefaultToolkit();
        Image icon = sist.getImage("C:\\Users\\USER\\Desktop\\sol\\2do cuatrimestre\\java\\src\\TrabajoIndividualProcesadorDeTexto\\txt.gif");
        setIconImage(icon);

        setVisible(true);
        setResizable(false);

        addWindowListener(new EventWindows());
    }

    private static class EventWindows implements WindowListener {

        @Override
        public void windowOpened(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowClosing(WindowEvent e) {
            if (!LaminaMenu.guardado) {
                int reply = JOptionPane.showConfirmDialog(null, "Desea Guardar?", "Esta Saliendo sin guardar", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {
                    if (LaminaMenu.guardado) {
                        LaminaMenu.guardarDatosRapido();
                    } else {
                        LaminaMenu.guardarDatos();
                    }
                }
                else {
                    System.exit(0);
                }

            }
        }

        @Override
        public void windowClosed(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowIconified(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowDeiconified(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowActivated(WindowEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowDeactivated(WindowEvent e) {
            // TODO Auto-generated method stub

        }

    }
}
class LaminaMenu extends JPanel{

    public LaminaMenu(){

        setLayout(new BorderLayout());

        JPanel laminaMenu = new JPanel();

        //Crea barra de menues
        JMenuBar mibarra = new JMenuBar();

        //Crea cada menu
        JMenu archivo = new JMenu("Archivo");
        fuente = new JMenu("Fuente");
        estilo = new JMenu("Estilo");
        tamanio = new JMenu("Tamaño");

        //Crea submenu del menu archivo
        JMenuItem guardar = new JMenuItem("Guardar");
        JMenuItem abrir = new JMenuItem("Abrir");

        //Agrega dos submenu al menu archivo
        archivo.add(guardar);
        archivo.add(new JSeparator()); //separador entre los submenus
        archivo.add(abrir);

        //Permite abrir o guardar el archivo
        abrir.addActionListener(new abrirArchivo());
        guardar.addActionListener(new guardarArchivo());
        guardar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_DOWN_MASK));


        //Crea submenu y los agrega al menu fuente
        gestorMenu("Arial", "Fuente", "Arial",9,12, "");
        gestorMenu("Calibri", "Fuente", "Calibri",9,12, "");
        gestorMenu("Times New Roman", "Fuente", "Times New Roman",9,12, "");

        //Crea submenu y los agrega al menu estilo
        gestorMenu("Negrita", "Estilo", "", Font.BOLD,1, "C:\\Users\\USER\\Desktop\\sol\\2do cuatrimestre\\java\\src\\TrabajoIndividualProcesadorDeTexto\\negrita.gif");
        gestorMenu("Cursiva", "Estilo", "", Font.ITALIC,1, "C:\\Users\\USER\\Desktop\\sol\\2do cuatrimestre\\java\\src\\TrabajoIndividualProcesadorDeTexto\\cursiva.gif");

        //Crea submenu y los agrega al menu tamanio
        gestorMenu("12", "Tamaño", "",9,12, "");
        gestorMenu("15", "Tamaño", "",9,15, "");
        gestorMenu("20", "Tamaño", "",9,20, "");
        gestorMenu("25", "Tamaño", "",9,25, "");
        gestorMenu("50", "Tamaño", "",9,50, "");


        //Agrega cada menu a la barra de menues
        mibarra.add(archivo);
        mibarra.add(fuente);
        mibarra.add(estilo);
        mibarra.add(tamanio);

        //Agrega la barra de menu a la lamina
        laminaMenu.add(mibarra);
        add(laminaMenu, BorderLayout.NORTH);

        //Crea un area de texto
        texto = new JTextPane();
        JScrollPane laminaBarras = new JScrollPane(texto);
        add(laminaBarras, BorderLayout.CENTER);

    }
    public void gestorMenu(String nombreSubMenu, String menu, String tipoLetra, int estilos, int tam, String rutaImagen){

        JMenuItem elemMenu = new JMenuItem(nombreSubMenu, new ImageIcon(rutaImagen));

        switch (menu) {
            case "Fuente":

                fuente.add(elemMenu);
                switch (tipoLetra) {
                    case "Arial":

                        elemMenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiaLetra", "Arial"));

                        break;
                    case "Calibri":

                        elemMenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiaLetra", "Calibri"));

                        break;
                    case "Times New Roman":

                        elemMenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiaLetra", "Times New Roman"));

                        break;
                }

                break;
            case "Estilo":

                estilo.add(elemMenu);
                if (estilos == Font.BOLD) {

                    elemMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
                    elemMenu.addActionListener(new StyledEditorKit.BoldAction());

                } else if (estilos == Font.ITALIC) {

                    elemMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
                    elemMenu.addActionListener(new StyledEditorKit.ItalicAction());

                }

                break;
            case "Tamaño":

                tamanio.add(elemMenu);
                elemMenu.addActionListener(new StyledEditorKit.FontSizeAction("cambiaTamaño", tam));

                break;
        }
    }

    public static void cargarDatos () {
        try
        {
            JFileChooser archivo1 = new JFileChooser();

            //abre el FileChoose sobre el JTextPane
            archivo1.showOpenDialog(texto);

            //guarda la direccion del archivo1
            String arch = archivo1.getSelectedFile().getAbsolutePath();

            //direccion del archivo1 style
            String archStyle = archivo1.getCurrentDirectory() +"\\"+ archivo1.getSelectedFile().getName() + "Style";

            ObjectInputStream leerFichero = new ObjectInputStream(new FileInputStream(arch));
            ObjectInputStream leerFicheroStyle = new ObjectInputStream(new FileInputStream(archStyle));

            //lee el archivo1 para traer el texto
            texto.setText((String) leerFichero.readObject());
            //lee el archivoStyle para traer el formato
            texto.setStyledDocument((StyledDocument) leerFicheroStyle.readObject());

            leerFichero.close();
            leerFicheroStyle.close();
            guardado = false;
        }
        catch (IOException ioe)
        {
            JOptionPane.showConfirmDialog(null, "ACCESO DENEGADO", "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException c)
        {
            System.out.println("Class not found");
            c.printStackTrace();
        }
    }

    //Permite Abrir desde el menu archivo
    private static class abrirArchivo implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            //llama al metodo cargarDatos
            LaminaMenu.cargarDatos();

        }
    }

    public static void guardarDatos () {
        try
        {
            JFileChooser elegirArchivo = new JFileChooser();
            elegirArchivo.showSaveDialog(texto);

            //toma la direccion
            File arch1 = elegirArchivo.getCurrentDirectory();

            //usa la direccion anterior para poder generar el path completo
            String ruta = arch1+"\\"+elegirArchivo.getSelectedFile().getName();

            //usa la direccion anterior para poder generar el path completo para el arch1 style
            String rutaStyle = arch1+"\\"+elegirArchivo.getSelectedFile().getName()+"Style";

            //guarda esas rutas en nuestras variables estaticas para poder utilizarlas luego
            rutaGuardada = ruta;
            rutaGuardadaStyle = rutaStyle;

            ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(ruta) );
            ObjectOutputStream escribiendoFicheroStyles = new ObjectOutputStream(new FileOutputStream(rutaStyle) );

            //guarda el texto
            escribiendoFichero.writeObject(texto.getText());

            //guarda un StyleDocument
            escribiendoFicheroStyles.writeObject(texto.getStyledDocument());
            escribiendoFichero.close();
            escribiendoFicheroStyles.close();

            JOptionPane.showConfirmDialog(null, "Archivos Guardados", "Exito", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE);
            guardado = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Permite la soobreescritura en archivos ya creados
    public static void guardarDatosRapido () {
        try
        {
            //usa las variables del anterior guardado para directamente sobreescribir los archivos
            ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(rutaGuardada) );
            ObjectOutputStream escribiendoFicheroStyles = new ObjectOutputStream(new FileOutputStream(rutaGuardadaStyle) );

            escribiendoFichero.writeObject(texto.getText());
            escribiendoFicheroStyles.writeObject(texto.getStyledDocument());

            escribiendoFicheroStyles.close();
            escribiendoFichero.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Permite guardar desde el menu archivos
    private static class guardarArchivo implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (guardado) {
                LaminaMenu.guardarDatosRapido();
            } else {
                LaminaMenu.guardarDatos();
            }

        }
    }

    //Guardan las rutas del guardado
    private static String rutaGuardada = "";
    private static String rutaGuardadaStyle = "";

    //Permite verificar si el archivo nuevo fue guardado o no
    public static boolean guardado = false;

    private static JMenu fuente;
    private static JMenu estilo;
    private static JMenu tamanio;

    private static JTextPane texto;
}