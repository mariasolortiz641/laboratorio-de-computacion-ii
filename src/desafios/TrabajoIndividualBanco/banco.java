import java.io.*;
import java.util.HashSet;
import java.util.Set;


public class banco {
    public static void main(String[]args){

        CuentaCorriente cta1 = new CuentaCorriente("Juan", 1000, 258741);
        CuentaCorriente cta2 = new CuentaCorriente("Rita", 2000, 951159);

        Set<CuentaCorriente> listadoDeCeuntas = new HashSet<>();
        listadoDeCeuntas.add(cta1);
        listadoDeCeuntas.add(cta2);

        Operaciones.Transferir(cta1, cta2, 500);

        System.out.println("Salida: "+listadoDeCeuntas.toString());

        try {
            ObjectOutputStream flujoDeSalida = new ObjectOutputStream(new FileOutputStream(
                    "C:\\Users\\USER\\Desktop\\sol\\2do cuatrimestre\\java\\src\\Banco\\src"));

            flujoDeSalida.writeObject(listadoDeCeuntas);
            flujoDeSalida.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {

            ObjectInputStream flujoDeEntrada = new ObjectInputStream(new FileInputStream(
                    "C:\\Users\\USER\\Desktop\\sol\\2do cuatrimestre\\java\\src\\Banco\\src\\listadoDeCeuntas.dat"));

            Set<CuentaCorriente> listadoDeCeuntasEntrada = (Set<CuentaCorriente>) flujoDeEntrada.readObject();

            System.out.println("Entrada: "+ listadoDeCeuntasEntrada.toString());

            flujoDeEntrada.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}

class Operaciones{

    public static void Transferir(CuentaCorriente cuentaIngreso, CuentaCorriente cuentaEgreso, double monto){
        cuentaEgreso.sacarDinero(monto);
        cuentaIngreso.igresarDinero(monto);
    }

}