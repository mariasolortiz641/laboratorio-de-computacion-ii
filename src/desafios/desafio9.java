package desafios;

import java.util.Scanner;

public class desafio9 {
    public static void main (String[] args) {
        Scanner entrada = new Scanner(System.in);

        char sexo;
        double altura, pesoideal;

        System.out.println("Introduce tu sexo F = mujer; M = hombre en mayuscula");
        sexo = entrada.next().charAt(0);

        System.out.println("Introduce tu altura en cm:");
        altura = entrada.nextDouble();

        if (sexo == 'F'){
            pesoideal = altura - 120;
            System.out.println("Tu peso ideal es:"+pesoideal+" kg");
        }
        else {
            pesoideal = altura - 110;
            System.out.println("Tu peso ideal es:"+pesoideal+" kg");
        }
    }
}
