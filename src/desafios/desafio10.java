package desafios;

import java.util.Scanner;

public class desafio10 {
    public static void main (String[] args) {
        Scanner entrada = new Scanner(System.in);

        int nroaleatorio = (int)(Math.random()*100+1);
        int nro;
        int intentos=0;

        System.out.println("-----------------------Juego--------------------------");
        System.out.println("Debe adivinar un número entero aleatorio entre 1 y 100");

        do{
            intentos++;

            System.out.print("Introduce un número:");
            nro = entrada.nextInt();

            if (nroaleatorio<nro){
                System.out.println("El número entero aleatorio es menor al ingresado");
            }
            else
            if (nroaleatorio > nro) {
                System.out.println("El número entero aleatorio es mayor al ingresado");
            }
        }while(nro!=nroaleatorio);
        System.out.println("Correcto!");
        System.out.println("Número de intentos consumidos: "+intentos);
        System.out.println("Número generado: "+nro);
    }
}
