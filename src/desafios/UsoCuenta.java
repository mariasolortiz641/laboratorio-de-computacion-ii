package desafios;

import java.util.Random;

public class UsoCuenta {
    public static void main (String[] args) {

        CuentaCorriente cuenta1 = new CuentaCorriente("Sol Ortiz", 50000);
        CuentaCorriente cuenta2 = new CuentaCorriente("Juan Perez", 10000);

        System.out.println("Datos iniciales de las cuentas\n");
        System.out.println("Cuenta 1: \n"+cuenta1.toString());
        System.out.println("Cuenta 2: \n"+cuenta2.toString());


        CuentaCorriente.Transferencia(cuenta1, cuenta2, 5000);

        System.out.println("\nDatos finales de las cuentas\n");
        System.out.println("Cuenta 1: \n"+cuenta1.toString());
        System.out.println("Cuenta 2: \n"+cuenta2.toString());

    }
}
class CuentaCorriente {

    private double saldo;
    private String nombreTitular;
    private long numeroCuenta;

    public CuentaCorriente(String nombreTitular, double saldo){

        this.nombreTitular = nombreTitular;
        this.saldo = saldo;

        Random NroCuenta = new Random();
        this.numeroCuenta = Math.abs(NroCuenta.nextLong());
    }
    public String IngresarDinero(double dinero){
        if (dinero > 0){
            this.saldo+= dinero;
            return "Dinero ingresado con exito: "+dinero;
        }
        else {
            return "No se pudo realizar la operación, ya que ingreso un numero negativo";
        }
    }
    public String EstraerDinero(double dinero){
        if (dinero <= saldo) {
            this.saldo -= dinero;
            return "Extracción exitosa";
        }
        else
            return "Su saldo es insuficiente para realizar esta operación";
    }

    public double getSaldo() {
        return saldo;
    }

    @Override
    public String toString() {
        return "desafios.CuentaCorriente{" +
                "saldo=" + saldo +
                ", nombreTitular='" + nombreTitular + '\'' +
                ", numeroCuenta=" + numeroCuenta +
                '}';
    }
    public static void Transferencia(CuentaCorriente CuentaOrigen, CuentaCorriente CuentaDestino, double dinero){

        if (dinero <= CuentaOrigen.saldo){
            CuentaDestino.saldo += dinero;
            CuentaOrigen.saldo -= dinero;
        }
        else
            System.out.print("Su saldo es insuficiente para realizar esta operación");
    }
}
