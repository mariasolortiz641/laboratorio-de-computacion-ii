package DesafioIndividual6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import javax.imageio.*;

public class Windows {

    public static void main(String[] args) {
	// write your code here

        Ventana panel=new Ventana();

    }
}
class Ventana extends JFrame {

    public Ventana(){

        //Extrae el tamanio del monitor
        Toolkit mipantalla=Toolkit.getDefaultToolkit();
        Dimension tamanioPantalla=mipantalla.getScreenSize();

        //Extrae la altura y el ancho del monitor
        int altPantalla=tamanioPantalla.height;
        int anchoPantalla=tamanioPantalla.width;

        //Tamanio del panel
        setSize(anchoPantalla/3,altPantalla/3);
        //Lugar donde va a aparecer el panel
        setLocation(anchoPantalla/3,altPantalla/3);

        //Hace visible el panel/ventana
        setVisible(true);
        //Cierra el proceso al precionar la "x" en el panel
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Cambia el tamanio del panel, en este caso no esta permitido
        setResizable(false);
        //Titulo del panel
        setTitle("Panel");

        //cambia el icono del panel
        Image miIcono=mipantalla.getImage("C:\\Users\\USER\\Desktop\\sol\\2do cuatrimestre\\java\\src\\DesafioIndividual6\\imagen.gif");
        setIconImage(miIcono);

        Panel panel=new Panel();
        add(panel);
        panel.setBackground(Color.BLACK);

    }
}
class Panel extends JPanel {

    JButton boton1 = new JButton("Ingresar");
    JButton boton2 = new JButton("Cancelar");

    private Image imagen;

    public Panel() {

        add(boton1);
        add(boton2);
        EventoFoco foco = new EventoFoco();
        boton1.addFocusListener(foco);
        boton2.addFocusListener(foco);
        EventoRaton mouse = new EventoRaton();
        addMouseListener(mouse);
    }

    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;

        Font fuente = new Font("Helvetica", Font.ITALIC, 30);
        g2.setFont(fuente);

        g2.drawString("Hola Mundo!!", 150, 50);

        boton1.setBounds(130, 70, 100, 20);
        boton2.setBounds(260,70,100,20);

        File miimagen=new File("C:\\Users\\USER\\Desktop\\sol\\2do cuatrimestre\\java\\DesafioIndividual6\\hola.png");

        try{
            imagen=ImageIO.read(miimagen);
        } catch (IOException e){
            System.out.println("No se encontró la imagen");
        }
        g2.drawImage(imagen,150,100,null);
    }

    private class EventoFoco implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {
            if(e.getSource() == boton1){
                System.out.println("Boton Ingresar ha ganado foco");
            }
            else{
                System.out.println("Boton Cancelar ha ganado foco");
            }
        }

        @Override
        public void focusLost(FocusEvent e) {
            if(e.getSource() == boton1){
                System.out.println("Boton Ingresar ha perdido foco");
            }
            else{
                System.out.println("Boton Cancelar ha perdido foco");
            }
        }
    }
    static class EventoRaton implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
