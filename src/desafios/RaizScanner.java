package desafios;

import java.util.Scanner;

public class RaizScanner {
    public static void main (String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.println("introduce un número:");
        int nro = entrada.nextInt();

        System.out.print("La raiz cuadrada de "+nro+" es = "+Math.sqrt(nro));
    }
}
