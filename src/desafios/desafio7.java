package desafios;

public class desafio7 {
    public static void main (String[] args) {
        double coseno = Math.cos(2.5);
        double seno = Math.sin(2.5);
        double tangente = Math.tan(2.5);
        double atan = Math.atan(1);
        double atan2 = Math.atan2(2,5);

        System.out.println("Funciones trigonometricas:");
        System.out.println("cos = "+coseno);
        System.out.println("sen = "+seno);
        System.out.println("tan = "+tangente);
        System.out.println("atan = "+atan);
        System.out.println("atan2 = "+atan2);

        System.out.println(" ");

        double exponencial = Math.exp(2);
        double logaritmos = Math.log(exponencial);

        System.out.println("Funcion exponencial y su inversa:");
        System.out.println("exp = "+exponencial);
        System.out.println("log = "+logaritmos);

        System.out.println(" ");

        final double pi = Math.PI;
        final double e = Math.E;

        System.out.println("Las dos constantes PI y e:");
        System.out.println("pi = "+pi);
        System.out.println("e = "+e);
    }
}
