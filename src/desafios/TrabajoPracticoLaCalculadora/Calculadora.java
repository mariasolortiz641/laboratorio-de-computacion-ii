package TrabajoPracticoLaCalculadora;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;


public class Calculadora {

    public static void main(String[] args) {

        Window ventana = new Window();

        ventana.setVisible(true);

        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ventana.setResizable(false);

    }
}
class Panel extends JPanel{

    private static final long serialVersionUID = 1L;

    private final JButton boton =new JButton("0");
    private final JPanel panelNum = new JPanel();
    private boolean inicio=true;
    private String calcular;
    private double acum;


    public Panel() {

        setLayout(new BorderLayout());
        boton.setEnabled(false);
        add(boton,BorderLayout.NORTH);
        panelNum.setLayout(new GridLayout(4,4));
        limpiarPantalla limpiarPantalla = new limpiarPantalla();
        anadirNumeros anadirNumeros = new anadirNumeros();
        Operadores operador = new Operadores();


        //Fila 1
        botones("7", anadirNumeros);
        botones("8", anadirNumeros);
        botones("9", anadirNumeros);
        botones("x", operador);

        //Fila 2
        botones("4", anadirNumeros);
        botones("5", anadirNumeros);
        botones("6", anadirNumeros);
        botones("-", operador);

        //Fila 3
        botones("1",anadirNumeros);
        botones("2",anadirNumeros);
        botones("3",anadirNumeros);
        botones("+", operador);

        //Fila 4
        botones("/", operador);
        botones("0",anadirNumeros);
        botones("AC", limpiarPantalla);
        botones("=", operador);

        add(panelNum);
        calcular ="=";

    }
    private class anadirNumeros implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String num = e.getActionCommand();
            if(inicio){
                boton.setText("");
                inicio =false;

            }
            boton.setText(boton.getText()+num);
        }
    }

    private class Operadores implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String user = e.getActionCommand();
            calcular(Double.parseDouble(boton.getText()));
            calcular =user;
            inicio =true;

        }

        public void calcular(Double num){
            try{
                switch (calcular) {
                    case "+":
                        acum += num;
                        break;
                    case "-":
                        acum -= num;
                        break;
                    case "x":
                        acum *= num;
                        break;
                    case "/":
                        acum /= num;
                        break;
                    case "=":
                        acum = num;
                        break;
                    case "AC":
                        acum = 0;
                        break;
                }
                boton.setText(""+ acum);
            } catch (ArithmeticException e){
                System.out.println("Dividir por 0 da un resultado infinito");
            }

        }
    }
    private class limpiarPantalla implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            boton.setText("0");
        }
    }

    private void botones(String nom, ActionListener listener){
        JButton btn = new JButton(nom);
        btn.addActionListener(listener);
        panelNum.add(btn);

    }
}

class Window extends JFrame {

    private static final long serialVersionUID = 1L;

    public Window() {

        setTitle("La Calculadora");

        Toolkit kit= Toolkit.getDefaultToolkit();

        Dimension screenSize=kit.getScreenSize();

        int height = screenSize.height;
        int width = screenSize.width;

        setSize(600,600);
        setLocation(width/4, height/6);

        Image miIcono=kit.getImage("C:\\Users\\USER\\Desktop\\sol\\2do cuatrimestre\\java\\src\\TrabajoPracticoLaCalculadora\\icono.gif");
        setIconImage(miIcono);

        Panel panel1= new Panel();
        add(panel1);

    }

}


